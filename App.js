import React, { Component } from "react";
import {Dimensions, FlatList, SafeAreaView,
    Text,Image,Button, AsyncStorage,
    TouchableOpacity,ScrollView, View} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

const screenHeight = Dimensions.get("window").height;

const MyStack = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                    options={{headerShown: false}}
                    name="Home"
                    component={HomeScreen}
                />
                <Stack.Screen options={{headerShown: false}} name="Profile" component={ProfileScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    );
};

export default class App extends Component{



    render(){
        return(
                <MyStack/>

        );
    }
}

class HomeScreen extends Component{
    constructor(props) {
        super(props);
        this.navigation = props.navigation;
        this.state = {
            books:[],
        };

    }
    componentDidMount(){

        AsyncStorage.getItem('data').then((res)=>{

            res && this.setState((prevState)=>{
                return {
                    books:JSON.parse(res).items
                }
            },()=>{
                console.log("ho gaya");
            })
        })


        fetch(
            `https://www.googleapis.com/books/v1/volumes?q=ocean`,
            {
                method: "GET",
                headers: new Headers({
                    Accept: "application/vnd.github.cloak-preview"
                })
            }
        )
            .then(res => res.json())
            .then(response => {
                AsyncStorage.setItem('data',JSON.stringify(response));
                console.log(response.items);
                this.setState((prevState)=>{
                    return {
                        books: response.items
                    }
                },()=>{
                    console.log("ho gaya");
                })
            })
            .catch(error => {
                console.log(error);

            });
    }
    render() {
        return (
            <SafeAreaView style={{flex: 1}}>

                <ScrollView>
                    {
                        this.state.books && this.state.books.map(item => {
                            return (
                                <TouchableOpacity style={{
                                    width: '100%',
                                    height: screenHeight * 0.2,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    marginVertical: 10
                                }}
                                                  onPress={() => {
                                                      this.navigation.navigate('Profile', { data: item })
                                                  }}>
                                    <View style={{flex: 1, width: '50%', alignItems: 'center', position: 'relative'}}>
                                        <View style={{
                                            backgroundColor: '#0004',
                                            flex: 1,
                                            width: '100%',
                                            justifyContent: 'flex-end',
                                            alignItems: 'center'
                                        }}>
                                            <Text style={{color:'#fff',fontSize:20}}>
                                                {item["volumeInfo"]["title"]?item["volumeInfo"]["title"] : 'Unknown'}
                                            </Text>
                                            <Text style={{color:'#fff',fontSize:20}}>
                                                { item["volumeInfo"]["authors"]?item["volumeInfo"]["authors"].slice(0,2) : "Unknown"}
                                            </Text>
                                        </View>
                                        {
                                            item["volumeInfo"] && item["volumeInfo"]["imageLinks"] && item["volumeInfo"]["imageLinks"]["thumbnail"] ?
                                                <Image
                                                    source={{uri: item["volumeInfo"]["imageLinks"]["thumbnail"]}}
                                                    style={[styles.imageCover,{
                                                        resizeMode: 'stretch'
                                                    }]}
                                                />
                                                :
                                                <View style={styles.imageCover}>
                                                    <Text>No Image Available</Text>
                                                </View>
                                        }

                                    </View>

                                </TouchableOpacity>
                            )
                        })
                    }
                </ScrollView>
            </SafeAreaView>
        );
    }
};
const ProfileScreen = ({ navigation, route }) => {
    return (
        <SafeAreaView style={{flex:1}}>
            <View style={{alignItems:'center'}}>
                {
                    route.params.data["volumeInfo"] &&
                    route.params.data["volumeInfo"]["imageLinks"] &&
                    route.params.data["volumeInfo"]["imageLinks"]["thumbnail"] ?
                        <View style={{height:screenHeight*0.3,width:'50%'}}>
                            <Image
                                source={{uri: route.params.data["volumeInfo"]["imageLinks"]["thumbnail"]}}
                                style={{
                                    resizeMode: 'stretch',
                                    flex:1,
                                }}
                            />
                        </View>
                        :
                        <Text>
                            No Image Available!
                        </Text>
                }




            </View>
            <View style={{alignItems:'center',marginTop:10}}>
                {
                    route.params.data["volumeInfo"]["title"] && <Text>Name : {route.params.data["volumeInfo"]["title"]}</Text>
                }


                {
                    route.params.data["volumeInfo"]["publisher"] && <Text>Publisher : {route.params.data["volumeInfo"]["publisher"]}</Text>
                }
                {
                    route.params.data["volumeInfo"]["pageCount"] && <Text>Page Count : {route.params.data["volumeInfo"]["pageCount"]}</Text>
                }

            </View>
        </SafeAreaView>
    );
};

const styles ={
  imageCover :  {
      zIndex: -1,
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0
  }
};

